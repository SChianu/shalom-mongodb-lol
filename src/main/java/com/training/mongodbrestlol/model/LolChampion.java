package com.training.mongodbrestlol.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="lol_champions")
public class LolChampion {

    @Id
    private String id;

    private String name;
    private String role;
    private String difficulty;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(String difficulty) {
        this.difficulty = difficulty;
    }

    @Override
    public String toString() {
        return "LOL Champion id: " + this.id + ", Name: " + this.name +
                ", Role: " + this.role + ", Difficulty: " + this.difficulty + ".";
    }
}
