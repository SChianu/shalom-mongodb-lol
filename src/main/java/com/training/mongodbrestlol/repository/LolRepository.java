package com.training.mongodbrestlol.repository;

import com.training.mongodbrestlol.model.LolChampion;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Component;

@Component //@Repository
public interface LolRepository extends MongoRepository<LolChampion, String> {

}
