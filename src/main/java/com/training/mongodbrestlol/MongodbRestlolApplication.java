package com.training.mongodbrestlol;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MongodbRestlolApplication {

    public static void main(String[] args) {
        SpringApplication.run(MongodbRestlolApplication.class, args);
    }

}
