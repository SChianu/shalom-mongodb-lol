package com.training.mongodbrestlol.controller;

import com.training.mongodbrestlol.model.LolChampion;
import com.training.mongodbrestlol.service.LolService;
import com.training.mongodbrestlol.service.LolServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin("*")
@RestController
@RequestMapping("/api/v1/lolChampion")
public class LolController {

    @Autowired
    private LolService lolService;

    @GetMapping
    public List<LolChampion> findAll() {
        return lolService.findAll();
    }

    @GetMapping("{id}")
    public LolChampion findById(@PathVariable String id) {
        return lolService.findById(id).get();
    }

    @DeleteMapping("{id}")
    public ResponseEntity delete(@PathVariable String id) {
        if (lolService.findById(id).isEmpty()) {
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }

        lolService.deleteById(id);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }



}
