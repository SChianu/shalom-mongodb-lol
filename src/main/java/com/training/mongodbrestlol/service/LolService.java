package com.training.mongodbrestlol.service;

import com.training.mongodbrestlol.model.LolChampion;
import com.training.mongodbrestlol.repository.LolRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component //@Service
public class LolService implements LolServiceInterface {

    @Autowired
    private LolRepository lolRepository;

    @Override
    public List<LolChampion> findAll() {
        return lolRepository.findAll();
    }

    @Override
    public Optional<LolChampion> findById(String id) {
        return lolRepository.findById(id);
    }

    @Override
    public LolChampion save(LolChampion champion) {
        return lolRepository.save(champion);
    }

    @Override
    public void deleteById(String id) {
        lolRepository.deleteById(id);
    }

}
