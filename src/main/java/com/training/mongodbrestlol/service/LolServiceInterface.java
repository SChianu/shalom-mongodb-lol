package com.training.mongodbrestlol.service;

import com.training.mongodbrestlol.model.LolChampion;

import java.util.List;
import java.util.Optional;

public interface LolServiceInterface {

    List<LolChampion> findAll();

    LolChampion save(LolChampion champion);

    void deleteById(String id);

    Optional<LolChampion> findById(String id);
}
